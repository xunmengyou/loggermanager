package com.wwy.Controller;


import com.alibaba.fastjson.JSON;
import com.wwy.Entity.Info;
import com.wwy.Service.LogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@Controller
@RequestMapping("/aspect")
public class LogController {


    @Autowired
    private LogService logService;

    @RequestMapping("/start.do")
    @ResponseBody
    public Object start(){
        return JSON.toJSONString("hello");
    }

    @RequestMapping("/getInfoById.do")
    @ResponseBody
    public Object getInfoById(Integer type){
        Info info = logService.getInfoById(1L,type);
        return JSON.toJSONString(info);
    }


    @RequestMapping("/insertInfo.do")
    @ResponseBody
    public Object insertInfo(){
        Info info = new Info();
        String userId = "11010101";
        info.setId(11L);
        info.setAge(23);
        info.setName("hello");
        logService.insertInfo(userId,info);
        return true;
    }

}
