package com.wwy.Service;

import com.wwy.Aspect.LogAnnotation;
import com.wwy.Entity.Info;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;


@Service
public class LogService {
    /**
     * 查询
     */
    @LogAnnotation(eventType = "getInfoById", detailArgs = {"序号"})
    public Info getInfoById(Long id,Integer type) {
        //具体的查库  业务操作
        if(type == 1){ //没有内存泄漏，只是大量计算，消耗内存
            List<Object> list = new ArrayList<>();
            for (int i = 0; i < 10000000; i++) {
                String str = "";
                for (int j = 0; j < 1000; j++) {
                    str += UUID.randomUUID().toString();
                }
                list.add(str);
            }
        }else if(type == 2){//死循环造成的
            while(true){
                String str = "";
            }
        }else if(type == 3){//死锁的例子
            Object a = new Object();
            Object b = new Object();
            new Thread(() -> {
                synchronized (a){
               //     System.out.println(Thread.currentThread().getName()+"已经获得a锁");
                    try {
                        Thread.sleep(5);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
               //     System.out.println(Thread.currentThread().getName()+"睡眠5ms结束");
                    synchronized (b) {
                        System.out.println(Thread.currentThread().getName()+"已经获得b锁");
                    }
                }
            },"线程1").start();
            new Thread(() -> {
                synchronized (b) {
               //     System.out.println(Thread.currentThread().getName() + "已经获得b锁");
                    try {
                        Thread.sleep(5);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
              //      System.out.println(Thread.currentThread().getName()+"睡眠5ms结束");
                    synchronized (a) {
                        System.out.println(Thread.currentThread().getName() + "已经获得a锁");
                    }
                }
            },"线程2").start();

        }

        return new Info();
    }
    /**
     * 某个用户 插入的记录
     */
    @LogAnnotation(eventType = "insertInfo", detailArgs = {"操作用户","插入内容如下:"})
    public void insertInfo(String userId,Info info){
      //具体插入库 业务操作
    }
    /**
     * 某个用户 删除记录操作
     */
    @LogAnnotation(eventType = "delInfo", detailArgs = {"操作用户","删除内容如下:"})
    public void delInfo(String userId, Info person) {
        //具体删除库 业务操作
    }
}
