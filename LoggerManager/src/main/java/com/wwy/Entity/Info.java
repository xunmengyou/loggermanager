package com.wwy.Entity;

import java.io.Serializable;

public class Info implements Serializable {

    private static final long serialVersionUID = 6517079744068859487L;
    private Long id;
    private String name;
    private Integer age;

    public Info(){
    }
    public Info(Long id,String name,Integer age){
        this.id = id;
        this.name = name;
        this.age = age;
    }
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    @Override
    public String toString(){
        return "id:" + id + " name:"+ name+" age:" + age;
    }
}
