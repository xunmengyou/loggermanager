package com.wwy.Aspect;


import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;
import java.lang.reflect.Method;

/**
 *类功能简述:	日志切面
 * 类功能详述:业务模块的操作需要写日志，通过日志切面，无缝地将写日志功能插入到业务模块的方法中，
 * 而不需要修改原有业务代码，对于业务模块而言，日志的写入是透明的，可随时撤走。
 *
 * 增、删、改 -----自定义注解配置需要以下面的开头
 * 增： insert  [String, Class] ---用户userId新增Class信息
 * 删除:del [String,Class] --用户userId删除Class信息
 * 改：modify [String, Class1,Class2] --用户userId修改class1变成class2
 */
@Aspect
@Component
public class LogAnnotationAspect {

    /**
     *  定义事件日志切入点，凡是应用了LogAnnotation注解的方法都会被写日志
     */
    @Pointcut("@annotation(com.wwy.Aspect.LogAnnotation)")
    public void eventLog() {}

    @AfterReturning(pointcut = "eventLog()")
    public void doAfterReturning(JoinPoint jp) {
        //具体的目标/代理方法
        Method proxyMethod = ((MethodSignature)jp.getSignature()).getMethod();
        //自定义注解的信息
        LogAnnotation proxyAnnotation = proxyMethod.getAnnotation(LogAnnotation.class);
        /**
         * 根据 传参值和字段定义，拼接信息，然后入库
         *
         * jp.getArgs():具体类的目标方法的传参值数据【每次请求，值不同】
         *  proxyAnnotation.detailArgs(): 是目标方法的自定义注解配置detailArgs的值
         */
        String detail = processLog(proxyMethod,jp.getArgs(), proxyAnnotation.detailArgs());
        //将这个信息入库即可
        System.out.println("detail :"+detail);
    }


    /**
     *
     * @param objParam    目标方法的参数
     * @param detailArgs  detailArgs
     * @return
     */
    private String processLog(Method proxyMethod,Object[] objParam, String[] detailArgs) {
        if(null == proxyMethod || null == objParam){
           return "";
        }
        StringBuilder sb = new StringBuilder();
        if(proxyMethod.getName().startsWith("insert")){
            String userId = (String)objParam[0];
            String value = objParam[1].toString();
            sb.append(detailArgs[0]).append(userId).append(detailArgs[1]).append(value);
        }else if(proxyMethod.getName().startsWith("del")){
            //具体业务自己写

        }else if(proxyMethod.getName().startsWith("modify")){
            //具体业务自己写
        }
        return sb.toString();
    }
}
